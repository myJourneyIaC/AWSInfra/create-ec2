
resource "aws_security_group_rule" "ingress_rules" {
  for_each          = var.sg_ingress_rules
  type              = "ingress"
  from_port         = each.value.from
  to_port           = each.value.to
  protocol          = each.value.proto
  cidr_blocks       = [ each.value.cidr ]
  description       = each.value.desc
  security_group_id = aws_security_group.custom-sg.id
}

resource "aws_security_group" "custom-sg" {
  name   = "${local.name}-sg"
  vpc_id = var.instance_vpc

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
