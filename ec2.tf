
resource "aws_iam_instance_profile" "custom_profile" {
  name = "${local.name}_profile"
  role = aws_iam_role.role.name
}

resource "aws_iam_role" "role" {
  name               = "${local.name}_role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_instance" "ec2" {

  for_each = toset(var.name)

  ami                         = var.image_id
  instance_type               = var.instance_type
  subnet_id                   = var.instance_subnet
  associate_public_ip_address = var.associate_public_ip_address 
  iam_instance_profile        = var.aws_iam_instance_profile

  key_name = var.aws_key

  security_groups = [ aws_security_group.custom-sg.id ]

  user_data = <<-EOF
    #!/bin/bash
      sudo yum install -y tmux gcc openssl-devel libyaml-devel libffi-devel readline-devel zlib-devel gdbm-devel ncurses-devel ruby-devel gcc-c++ jq git curl python python-pip; 
      git config --global credential.helper '!aws codecommit credential-helper ' && git config --global credential.UseHttpPath true
  EOF
 
  root_block_device {
    delete_on_termination = true
    volume_size = 50
    volume_type = "gp2"
  }

  tags = {
    Name = each.value
  }
  
}
