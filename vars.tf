
variable "name" {
  type = list
  default = [""]
}

variable "image_id" {
  default     = ""
}

variable "associate_public_ip_address" {
  type = bool
  default = false
}

variable "instance_type" {
  default     = ""
}

variable "instance_subnet" {
  default     = ""
}

variable "instance_vpc" {
  default     = ""
}

variable "aws_iam_instance_profile" {
  default = ""
}

variable "security_groups" {
  type = list
  default = [""]
}

variable "aws_key" {
  default = ""
}

variable "instance_username" {
  default = ""
}

variable "path_to_private_key" {
  default = ""
}

variable "sg_ingress_rules" {
    type   = map(map(any))
    default = {
       ssh   = { from=22, to=22, proto="tcp", cidr="0.0.0.0/0", desc="custom" }
    }
}
