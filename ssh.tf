
resource "aws_key_pair" "ec2_key" {
	key_name = var.aws_key
	public_key = file("${path.root}/file/${var.aws_key}.pub")
}
